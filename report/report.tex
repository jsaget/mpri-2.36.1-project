\documentclass[a4paper]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[main=english,french]{babel}

\usepackage{hyperref}
\hypersetup{
  colorlinks=true,
  linkcolor=blue,
  filecolor=magenta,      
  urlcolor=cyan,
}

\usepackage{enumitem}

\usepackage{minted}
\usemintedstyle{emacs}

\usepackage{amsthm, amsmath, amssymb}

\usepackage[margin=2cm]{geometry}
\usepackage{pdflscape}

% Required by the why3seesion table
\usepackage{longtable}
\usepackage{booktabs}
\usepackage{xcolor}
\usepackage{colortbl}
\usepackage{rotating}

\newcommand{\provername}[1]{\cellcolor{yellow!25}
  \begin{sideways}\textbf{#1}~~\end{sideways}}
\newcommand{\explanation}[1]{\cellcolor{yellow!13}lemma \texttt{#1}}
\newcommand{\transformation}[1]{\cellcolor{yellow!13}transformation \texttt{#1}}
\newcommand{\subgoal}[2]{\cellcolor{yellow!13}subgoal #2}
\newcommand{\valid}[1]{\cellcolor{green!13}#1}
\newcommand{\unknown}[1]{\cellcolor{red!20}#1}
\newcommand{\invalid}[1]{\cellcolor{red!50}#1}
\newcommand{\timeout}[1]{\cellcolor{red!20}(#1)}
\newcommand{\outofmemory}[1]{\cellcolor{red!20}(#1)}
\newcommand{\noresult}{\multicolumn{1}{>{\columncolor[gray]{0.8}}c|}{~}}
\newcommand{\failure}{\cellcolor{red!20}failure}
\newcommand{\highfailure}{\cellcolor{red!50}FAILURE}

\title{{\normalsize \textsc{MPRI 2.36.1 --- Proof of Program}}\\
  Solving Takuzu puzzles
}
\author{Jules Saget}
\date{February 25, 2021}

\begin{document}

\maketitle

The present document is the report of my submission for the programming project
of the MPRI\footnote{\emph{Master Parisien de Recherche en Informatique}
  (Parisian Master of Research in Computer Science)} course ``Proof of
Program''\footnote{\url{https://marche.gitlabpages.inria.fr/lecture-deductive-verif/}}.
Both the
subject\footnote{\url{https://marche.gitlabpages.inria.fr/lecture-deductive-verif/takuzu.pdf}}
and a skeleton
file\footnote{\url{https://marche.gitlabpages.inria.fr/lecture-deductive-verif/takuzu.zip}}
can be found on the course web page.

My submission, as well as this report, can be found on a git
repository\footnote{\url{https://git.eleves.ens.fr/jsaget/mpri-2.36.1-project}}.
A documented version of my code is also provided in
appendix~\ref{solving-takuzu-puzzles}.
The state of the Why3 session (with the prover output for each goal) is
provided in appendix~\ref{sec:results}.

\emph{Note: this report follows the structure of the subject.
  Some sections of the subject did not contain questions, so they do not have a
  counterpart in this report.
  This is why some section numbers are skipped sometimes.}

\tableofcontents

\setcounter{section}{1}

\section{Appetizers: Basic Functions on Arrays}
\label{sec:appetizers}

\subsection{Check for consecutive zeros}

\begin{enumerate}
\item The predicate $no\_3\_consecutive\_zeros\_sub$ is simply the
  following:
  \begin{align*}
         & no\_3\_consecutive\_zeros\_sub(a,l)\\
    \iff & \forall i\in\mathbb{Z}, 0 \leq i < l-2 \implies \neg(a[i] \wedge a[i+1] \wedge a[i+2])
  \end{align*}

  The predicate $no\_3\_consecutive\_zeros$ derives naturally:
  \begin{align*}
         & no\_3\_consecutive\_zeros(a) \\
    \iff & no\_3\_consecutive\_zeros\_sub\left(a, Array.length(a)\right)
  \end{align*}

\item This function was already implemented.
  Its code can be seen in~\ref{question-2}.

  The contract of this function is:
  \mint{ocaml}|ensures { result = True <-> no_3_consecutive_zeros a }|
  It captures the fact that this function checks whether the array has no three
  consecutive zeros.
  
  To prove this code  correct, I need to add this loop invariant:
  $no\_3\_consecutive\_zeros\_sub(a,(i+2))$.
  This invariant capture the fact that the sub-array the program already
  checked has no three consecutive zeros.
  It is initially true because the antecedent of the predicate is always false
  (when $i=2$ there is no $j$ such that $0 \leq j < i-2$).
  It is preserved at each iteration because the algorithm performs the only test
  that decides $no\_3\_consecutive\_zeros\_sub(a,(i+3))$ and is not
  decided by $no\_3\_consecutive\_zeros\_sub(a,(i+2))$.

  The post-condition then follows by definition of $no\_3\_consecutive\_zeros$.


\item This function was also already implemented.
  Its code can be seen in~\ref{question-3}.

  This function is very similar to the one before, and simply has a little
  performance upgrade.

  The contract of this function is the same as before.
  To prove it correct, I specified the meaning of $last1$ and
  $last2$:
  $last1 = a[i-1] \wedge last1 =
  a[i-2] \wedge no\_3\_consecutive\_zeros\_sub(a,i)$.
  The loop invariant is obviously initially true and preserved at each
  iteration, thus the post-condition holds.


\item This function was once again already implemented.
  Its code can be seen in~\ref{question-4}.

  The contract of this function is the same as before.
  To prove it correct, I needed a loop invariant that captured the precise
  meaning of $count\_zeros$:
  \begin{align*}
    & 0 \leq count\_zeros \leq 2\\
    \wedge &~count\_zeros \leq i\\
    \wedge &~count\_zeros = 1 \implies a[i-1] = 0\\
    \wedge &~i > 0 \implies a[i-1] = 0 \implies
             count\_zeros \geq 1\\
    \wedge &~i > 1 \implies
             a[i-1] = 0 = a[i-2] \iff
             count\_zeros = 2\\
    \wedge & \left( a[i] = 0 \wedge
             count\_zeros = 2 \right) \implies
             \neg \left(
             no\_3\_consecutive\_zeros\_sub(a,(i + 1))
             \right)\\
    \wedge & ~no\_3\_consecutive\_zeros\_sub(a,i)
  \end{align*}
  I believe this invariant is self-explanatory.

\end{enumerate}

\subsection{Checking for Same Number of Zeros and Ones}

\begin{enumerate}[resume]
\item The definition of \texttt{num\_occ} is simply (also available in~\ref{question-5}):
\begin{minted}{ocaml}
let rec num_occ (e:int) (f:int -> int) (i j:int) =
  if j <= i then 0 else
  if f (j-1) = e then 1 + num_occ e f i (j-1) else num_occ e f i (j-1)
\end{minted}

  The termination derives from the variant $j - i$.
\end{enumerate}

For the next two questions, the corresponding code is available in~\ref{questions-6-and-7}.

\begin{enumerate}[resume]
\item The post condition of \texttt{count\_number\_of} is as one would expect it
  to look like:
\begin{minted}{ocaml}
ensures { result = num_occ e a.elts 0 a.length }
\end{minted}
  This allows CVC4 to prove \texttt{same\_number\_of\_zeros\_and\_ones} automatically.

\item To prove this function, I had to specify an invariant that captured its
  behaviour:
  \begin{align*}
    &n = num\_occ(e,a.elts,0,i)\\
    \wedge &~a.elts~i = e \iff
             num\_occ(e,a.elts,0,(i+1)) =
             num\_occ(e,a.elts,0,i) + 1\\
    \wedge &~a.elts~i \neq e \iff
             num\_occ(e,a.elts,0,(i+1)) =
             num\_occ(e,a.elts,0,i)
  \end{align*}
  This invariant mimics closely the definition of \texttt{num\_occ}.

\end{enumerate}

\subsection{Checking for identical sub-arrays}

\begin{enumerate}[resume]
\item The complete definition is available in~\ref{question-8}.

  I also added a lemma, $identical\_sub\_arrays\_rec$, also available in~\ref{question-8}.
  This lemma provides a way to prove by induction that two sub-arrays are
  identical.

\item The annotated function is available in~\ref{question-9}.
  The contract of this function is:
\begin{minted}{coq}
requires { 0 <= o1 /\ o1 + l <= a.length }
requires { 0 <= o2 /\ o2 + l <= a.length }
requires { l>0 }
ensures { result = True <-> identical_sub_arrays a o1 o2 l }
\end{minted}
  The requirements are needed to prove that the accessed values in bound.
  The function ensures that it indeed check whether the two sub-arrays passed as
  arguments are identical.

  To prove it, I had to add a loop invariant:
  \begin{align*}
    &~0 \leq o1 + k \leq a.length\\
    \wedge &~0 \leq o2 + k \leq a.length\\
    \wedge &~k < l-1 \implies
             a[o1+ k] =
             a[o2+ k] \iff
             identical\_sub\_arrays(a,o1,o2,(k+1))\\
    \wedge &~identical\_sub\_arrays(a,o1,o2,k)
  \end{align*}
  This invariant provides the necessary hypotheses to use the
  $identical\_sub\_arrays\_rec$ lemma.
\end{enumerate}

\section{Specification of Takuzu puzzles}
\label{sec:takuzu}

\setcounter{subsection}{1}

\subsection{First Takuzu Rule for Chunks}

\begin{enumerate}[resume]
\item The definition of the predicate is the following (also available in~\ref{question-10}):
  \begin{align*}
    &~\forall k \in \mathbb{Z}, 0 \leq k < l-2 \\
    \implies &~g[start+k*incr] =
               g[start+(k+1)*incr] \\
    \implies &~g[start+k*incr] =
               g[start+(k+2)*incr] \\
    \implies &~g[start+k*incr] = Empty
  \end{align*}
  It simply states that if three adjacent squares have the same value, then this
  value must be $Empty$.

\item To prove this function, I took a lot of inspiration from the corresponding
  function in the \texttt{Appetizers} module.
  The list of invariants is with the full function body in~\ref{question-11}.
  The invariants are split into three categories:
  \begin{itemize}
  \item Invariants about $count\_zeros$:
\begin{minted}{coq}
invariant { 0 <= count_zeros <= 2 }
invariant { count_zeros <= i }
invariant { (count_zeros = 1 -> g[start + incr*(i-1)] = Zero) }
invariant {
  (i > 0 ->
  (g[start + incr*(i-1)] = Zero -> count_zeros >= 1))
}
invariant {
  (i > 1 ->
  (g[start + incr*(i-1)] = Zero = g[start + incr*(i-2)] <-> count_zeros = 2))
}
invariant {
  (g[start + incr*i] = Zero /\ count_zeros = 2 ->
  not no_3_consecutive_identical_elem g start incr (i+1))
}
\end{minted}
    These invariants are the translation of those in \texttt{Appetizers},
    adapted to the \texttt{takuzu\_grid} data structure.

    One notable difference is in the last invariant.
    This invariant now consist only in an implication, and is therefore not
    enough to prove the contract.
    Proving it is the job of the third category of invariants.

  \item Invariants about $count\_ones$ are identical to the previous invariants,
    only replacing the occurrences of \emph{zero} by \emph{one}.

  \item Invariants about the main property:
\begin{minted}{coq}
invariant { no_3_consecutive_identical_elem g start incr i }
invariant {
  i <= 7 ->
  no_3_consecutive_identical_elem g start incr (i+1) <-> (
    (not (g[start + incr*i] = One /\ count_ones = 2)) /\
    (not (g[start + incr*i] = Zero /\ count_zeros = 2))
  )
}
\end{minted}
    The first invariant is the main property we want to ensure.
    The second one ties together the properties of $count\_zeros$ and
    $count\_ones$, providing the key argument to prove that the main property is
    preserved.
  \end{itemize}
\end{enumerate}

\subsection{Second Takuzu Rule for Chunks}

\begin{enumerate}[resume]
\item The full function definition can be found in~\ref{question-12}.
  Its contract is
\begin{minted}{coq}
requires { g.length = 64 }
requires { 0 <= l <= 8 }
requires { valid_chunk start incr }
\end{minted}
  which simply captures the fact that the question has a meaning (the grid is of
  the right size, the chunk is valid and the number of elements we look at fit
  in a single chunk).
  To prove its termination, just notice that the value of $l$ is strictly decreasing.

  To prove the function \texttt{count\_number\_of} correct, I added some specifications.
  \begin{itemize}
  \item[\textsc{Contract}] The function requires that the inputs are legal: $g$
    must be of length 64 and $(start, incr)$ must represent a valid chunk.
  \item[\textsc{Loop invariants}] I added the following invariants:
\begin{minted}{coq}
invariant { n = num_occ e g start incr i }
invariant {
  i < 8 ->
    (g[start + incr*i] =  e <->
    num_occ e g start incr (i+1) = num_occ e g start incr i + 1)
}
invariant {
  i < 8 ->
    (g[start + incr*i] <> e <->
    num_occ e g start incr (i+1) = num_occ e g start incr i)
}
\end{minted}
    These invariants capture the fact that \texttt{count\_number\_of} behaves
    like \texttt{num\_occ}, and thus yields the same results.
  \end{itemize}

\item As one may expect, the predicate is defined as (also available in~\ref{question-13}):
  \begin{align*}
    rule\_2\_for\_chunk(g, start, incr) \iff & num\_occ(Zero, g, start, incr, 8) \leq 4 \\
                                             & \wedge num\_occ(One, g, start, incr, 8) \leq 4
  \end{align*}

  This predicate is used in the function \texttt{check\_rule\_2\_for\_chunk}.
  This function is really simple, and thus the only annotations needed are the
  fact that the input is valid (the grid has the right size and the chunk is
  valid).
\end{enumerate}

\subsection{Third Takuzu Rule for Chunks}

\begin{enumerate}[resume]
\item By our definition, ``identical'' means that all values are equal and are
  different from $Empty$.
  The predicate follows:
  \begin{align*}
    & identical\_chunks(g, s_1, s_2, incr, l)\\
    \iff & \forall k \in \mathbb{Z}, 0 \leq k < l \implies acc(g,s_1,incr,k) = acc(g,s_2,incr,k) \neq Empty
  \end{align*}

  The full code for \texttt{check\_identical\_chunks} is available
  in~\ref{question-14}.
  Basically, it simply raises an error when it detects that the two chunks are
  different or that one of the chunks contains an $Empty$ cell:
\begin{minted}{ocaml}
let check_identical_chunks g start1 start2 incr =
  try
    for i=0 to 7 do
      match acc g start1 incr i, acc g start2 incr i with
      | Zero,Zero -> ()
      | One,One -> ()
      | _ -> raise DiffFound
      end
    done;
    True
  with DiffFound -> False
  end
\end{minted}
  The function once again only requires that the input is valid:
\begin{minted}{coq}
requires { g.length = 64 }
requires { valid_chunk start1 incr }
requires { valid_chunk start2 incr }
\end{minted}

  Since the function is quite straightforward, only one simple loop invariant is required:
\begin{minted}{coq}
invariant { identical_chunks g start1 start2 incr i }
\end{minted}

\item The full code for \texttt{check\_rule\_for\_column} if available in~\ref{question-15}.
  What the function does is checking, for each column that is not the argument,
  that this columns is different from the argument:
\begin{minted}{ocaml}
let check_rule_3_for_column (g:takuzu_grid) (start:int) =
  for i=0 to 7 do
    if i <> start then
      if check_identical_chunks g start i 8 then
        raise Invalid
  done
\end{minted}

  The function once again only requires that the arguments are valid:
\begin{minted}{coq}
requires { g.length = 64 }
requires { valid_chunk start 8 }
\end{minted}

  The loop invariant that I provided captures the meaning of the function: after
  $i$ steps, we know that the column we consider is different from any other
  column of index $k$ such that $0 \leq k < i$:
\begin{minted}{coq}
invariant {
  forall k. 0 <= k < i /\ k <> start ->
    not (identical_columns g start k)
}
\end{minted}
\end{enumerate}

\subsection{Checking Rules Satisfaction for a Given Cell}

\begin{enumerate}[resume]
\item The definitions can be found in~\ref{question-16}.
  The first and the second rule are self-explanatory: we just ask for
  \texttt{rule\_\textnormal{\{\texttt{1}, \texttt{2}\}}\_for\_chunk} to be
  satisfied on both the column and the row of the given cell.
  For the third rule, we have to introduce a universal quantification, because
  the row (resp. column) corresponding to the given cell must be different from
  every other row (resp. column).

\item The function \texttt{check\_at\_cell} is simply the combination of all the
  \texttt{check\_rule} functions.
  Once again, the function only requires the input to be valid, \emph{ie} $g$ is of the
  right size and $n$ point to a cell of $g$:
\begin{minted}{coq}
requires { g.length = 64 }
requires { 0 <= n < g.length }
\end{minted}

\item The complete code (with annotations) can be found
  in~\ref{questions-18-19-and-20}.
  This function requires that the input is valid, but also that the rules are
  already satisfied for any cell coming before $n$, and that the element $e$
  that will be written in $g[n]$ is not $Empty$\footnote{This condition can seem
  arbitrary, but in practice we only intend to use \texttt{check\_cell\_change}
  to write non$Empty$ values in $g$.}:
\begin{minted}{coq}
requires { g.length = 64 }
requires { 0 <= n < g.length }
requires { e <> Empty }
requires { valid_up_to (g[n<-Empty]) n }
\end{minted}

  Proving the assertion is difficult, because the provers do not realise that
  $g[n \leftarrow Empty]$ and $g$ are almost the same array.

  Proving the post-condition is difficult because of the same reason.

\item I added the lemma \texttt{frame\_valid\_up\_to} that states that two
  arrays with the same elements are indistinguishable by \texttt{valid\_up\_to}:
  \begin{align*}
    \forall g_0, g_1, same\_grid(g_0, g_1) \implies \forall n \in \mathbb{Z}, 0 \leq n < g_0.length \implies valid\_up\_to(g_0,n) \iff valid\_up\_to(g_1,n)
  \end{align*}
  where $same\_grid$ means that the two arrays are valid takuzu grids with the
  same elements:
  \[
    same\_grid(g_0, g_1) \iff g_0.length = g_1.length = 64 \wedge
    \left( \forall k, 0 \leq k < g_0.length \implies g_0[k] = g_1[k] \right)
  \]

  I proved this lemma formally: we just need to inline the definition of
  \texttt{valid\_up\_to} until we can apply hypotheses of the form $g_0[k] =
  g_1[k]$.

\item I added a lemma \texttt{valid\_up\_to\_change} that states that
  \[
    \forall g, n, k, e, 0 \leq k < g.length
    \implies valid\_up\_to(g[k \leftarrow e],n)
    \implies valid\_for\_cell(g,k)
    \implies valid\_up\_to(g,n)
  \]
  Informally, that lemma states the following: if we change the value of a cell
  of $g$, but that the new value of that cell makes the grid valid at that cell,
  then the property $valid\_up\_to$ is preserved to the updated $g$ for all
  $n$\footnote{The lemma actually states something a bit different (but more
    difficult to explain): we suppose that $valid\_up\_to$ holds when a given
    cell of $g$ is changed.
    If the old version of $g$ is still valid for the given cell, then we can
    deduce than $valid\_up\_to$ holds for that old version of $g$.}.

  I decided to add the post-condition $g = old(g)[n \leftarrow e]$ to help
  proving the main post-condition.
\end{enumerate}

\subsection{Proving the Main Algorithm}

\begin{enumerate}[resume]
\item This question was by far the hardest one.
  The full code with annotations can be found in~\ref{question-21}.

  Most of the challenges I faced were on the second \texttt{try} in the
  \texttt{Empty} match case.
  Indeed, \texttt{g} was already modified once by \texttt{solve\_aux}, and the
  provers could not guess what does the modified version of \texttt{g} look like.

  The first step I went through was to add the usual requirements, alongside
  those that were already given:
\begin{minted}{coq}
requires { g.length = 64 }
requires { 0 <= n <= g.length }
requires { full_up_to g n }
requires { valid_up_to g n }
\end{minted}
  Here, we allow $n=64$ because it is a special values meaning a solution has
  been found\footnote{More precisely, the first 64 cells of the grid are full,
    so the algorithm wants to fill the next cell.
    There is no new cell, thus the grid is full!}.

  The variant is simply $64-n$.

  The post-conditions are more interesting:
\begin{minted}{coq}
ensures { extends (old g) g }
ensures { valid_up_to g n }
\end{minted}
  I added the first post-condition because it was obviously true, and because I
  thought it could be useful to have it later (it will be!).
  The second post-condition is used to prove the pre-condition on the second
  call of \texttt{check\_cell\_change}: because $g$ was potentially modified by
  \texttt{solve\_aux}, we need to tell that this condition still holds.

  To fully prove the pre-condition of the second \texttt{check\_cell\_change}, I
  added the following exceptional post-condition to
  \texttt{check\_cell\_change}:
\begin{minted}{coq}
raises { Invalid -> g = (old g)[n <- e] }
\end{minted}
  This way, the program knows what \texttt{g} looks like when it passes it to
  \texttt{check\_cell\_change} for the second time.
  This is not enough though, because the provers cannot automatically deduce
  that the modified \texttt{g} is valid up to $n$.

  To make provers deduce the validity on their own this, I introduced two
  lemmas.
  The first one states that if the extension of a given grid is valid, then the
  original grid is valid:
  \[
    \forall g_1, g_2, extends(g_1, g_2) \implies \forall n, 0 \leq n < 64 \implies
    valid\_up\_to(g_2,n) \implies valid\_up\_to\_(g_1,n)
  \]
  The second lemma states that making a value empty realises an extension:
  \[
    \forall g,n, 0 \leq n < g.length \implies extends(g[n \leftarrow Empty],g)
  \]
  The first lemma (which is called \texttt{extension\_validity}) is left
  unproved.
  I think this lemma is obvious: erasing a number from the grid cannot create an
  invalid grid out of a valid one.

  With all these lemmas, the only thing that remained unproved was the
  exceptional post-condition.
  To prove it, I had to create a refined version of my second lemma:
  \[
    \forall g,n,e, 0 \leq n < g.length \implies
    extends(g[n \leftarrow e][n \leftarrow Empty],g)
  \]
  I cannot really explain the deep idea behind this lemma: I looked at the
  \emph{Task} tab of the Why3 IDE and noticed that this lemma would do the trick.
\end{enumerate}

\section{Extra Questions, Discussions, Conclusions}
\label{sec:extra}
% sec extra, sec extra, Sec Extra, SEC EXTRAAAAAAAAAA

\begin{enumerate}[resume]
\item All the proved lemmas are presented when they are most relevant, thus I
  have nothing more to say about them.
  
\item First, we can say that the algorithm should be complete: it tries every
  possible grid until it finds a valid one, and thus will it will find a valid
  grid if it exists.
  We could try to define an order $\prec$ on grids such that $g_1 \prec g_2$ if
  and only if $g_1$ is ``visited'' before $g_2$ by our program.
  This may require our \texttt{solve\_aux} to be split into smaller chunks in
  order to reason on those chunks and show that the program respects the order
  we defined.
  We will also need to specify that, if there is a solution that extends our
  base grid, then there is a solution $g_s$ that extends our grid $g_0$ such that
  $g_0 \prec g_s$.

\item The main difficulty I experienced was understanding why the provers were
  not behaving as I expected.
  In many cases, I did not know what to tell the provers, because the result
  seemed obvious to me.
  What I ended up doing in most cases is dig into the \emph{Task} tab of the IDE
  and try to do the proof myself\dots only to realise that the invariants I
  specified were too weak, and that some hypotheses were missing so as to prove
  the goal.

  My lack of experience with proof assistants was a setback when trying to prove
  technical lemmas.
  In several lemmas, the proof mainly consists in unfolding the definition of
  \texttt{valid\_for\_cell} to its bare bones.
  I believe that I could have created a simple Coq tactic which would have done
  this job for me.
  
  Finally, I should mention that I did not follow the class with great regularity.
  This is not due to the subject nor the teaching, I had trouble following most
  of the classes I took this year.
  I think me having trouble to follow the class is the explanation behind most
  of the difficulties I faced.

  Nevertheless, I really enjoyed this project, and I think I will continue to
  play with Why3 and try to dig into it.
\end{enumerate}

\newpage

\appendix
\input{code/all.tex}

\newpage

\section{Why3 Proof results}
\label{sec:results}

\subsection{Theory "Appetizers"}

\input{tables/Appetizers.tex}

\begin{landscape}
  \subsection{Theory "Takuzu"}
  
  \input{tables/Takuzu.tex}
\end{landscape}

\subsection{Theory "Test"}

\input{tables/Test.tex}

\end{document}
