SHELL := /bin/bash
LATEX := latexmk -pdf -pdflatex="pdflatex --shell-escape"
PYTHON := python

.PHONY: help
help: ## Show this help
	@egrep -h '\s##\s' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'



.PHONY: ide
ide: ## Launch Why3 IDE on file tazuku.mlw
	why3 ide takuzu.mlw



.PHONY: test
tests: test_takuzu.native ## Test the program on an example grid
	./test_takuzu.native

takuzu.ml: takuzu.mlw
	why3 extract -D ocaml64 takuzu.mlw -o takuzu.ml

test_takuzu.native: test_takuzu.ml takuzu.ml
	ocamlbuild -pkg unix -pkg zarith test_takuzu.native



.PHONY: replay
replay: ## Replay the proofs
	why3 replay takuzu



.PHONY: doc
doc: takuzu.html takuzu/why3session.html ## Produce html file

takuzu.html: takuzu.mlw
	why3 doc takuzu.mlw

takuzu/why3session.html: takuzu/why3session.xml
	why3 session html takuzu



.PHONY: report
report: report/report.pdf ## Produce pdf file

report/report.pdf: report/code/all.tex report/report.tex \
	report/tables/Appetizers.tex report/tables/Takuzu.tex \
	report/tables/Test.tex
	cd report && $(LATEX) report.tex

report/code/all.tex: takuzu.mlw scripts/edit_tex.py
	why3 doc --body-only --output report/code/ takuzu.mlw
	rm -f report/code/*.css
	pandoc -o report/code/all.tex report/code/takuzu.html
	sed --in-place 's/begin{verbatim}/begin{minted}{ocaml}/g' report/code/all.tex
	sed --in-place 's/end{verbatim}/end{minted}/g' report/code/all.tex
	$(PYTHON) scripts/edit_tex.py report/code/all.tex

report/tables/Appetizers.tex report/tables/Takuzu.tex report/tables/Test.tex: \
	takuzu/why3session.xml
	why3 session latex -style 2 -o report/tables -longtable takuzu



.PHONY: submission
submission: Saget-Jules.tar.gz ## Create the tarball to be sent for submission

Saget-Jules.tar.gz: takuzu.mlw takuzu/why3session.xml takuzu/why3shapes.gz takuzu/why3session.html report
	mkdir -p submission
	cp -u takuzu.mlw submission/
	cp -ru takuzu submission/
	rm -rf submission/takuzu/*.bak
	cp -u report/report.pdf submission/
	tar -czvf Saget-Jules.tar.gz submission

.PHONY: clean
clean: ## Clean repository
	rm -f takuzu.ml
	rm -f test_takuzu.native
	rm -f takuzu.html
	rm -f takuzu/why3session.html
	cd report && latexmk -C
	rm -f report/code/all.tex
	rm -f report/tables/*
	rm -f Saget-Jules.tar.gz
	rm -rf submission

.PHONY: all
all: doc report submission tests replay ## Generate all files, test and replay proofs
