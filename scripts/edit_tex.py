import sys

def process_chunk(chunk, counter=0):
    assert counter >= 0, "Counter is negative"
    new_chunk = []
    sub_chunks = chunk.split('{]}')
    for sub_chunk in sub_chunks[:-1]:
        new_chunk.append(sub_chunk)
        counter -= 1
        if counter == 0:
            new_chunk.append('}')
        else:
            new_chunk.append('{]}')
    new_chunk.append(sub_chunks[-1])
    new_chunk = ''.join(new_chunk)
    return new_chunk, counter

def process_line(line, counter=0):
    assert counter >= 0, "Counter is negative"
    new_line = []
    chunks = line.split('{[}')
    for chunk in chunks[:-1]:
        new_chunk, counter = process_chunk(chunk, counter)
        new_line.append(new_chunk)
        if counter == 0:
            new_line.append('\\texttt{')
        else:
            new_line.append('{[}')
        counter += 1
    new_last_chunk, counter = process_chunk(chunks[-1], counter)
    new_line.append(new_last_chunk)
    new_line = ''.join(new_line)
    return new_line, counter

def process_line_list(f):
    new_file = []
    counter = 0
    for line in f:
        new_line, counter = process_line(line, counter)
        new_file.append(new_line)
    assert counter == 0, "Counter != 0 after process"
    return new_file

def process_file(filename):
    my_file = open(filename, 'rt')
    line_list = my_file.readlines()
    my_file.close()
    new_file = process_line_list(line_list)
    my_file = open(filename, 'wt')
    for line in new_file:
        my_file.write(line)
    my_file.close()

if __name__ == "__main__":
    process_file(sys.argv[1])
